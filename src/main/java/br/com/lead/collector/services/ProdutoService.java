package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Iterable<Produto> buscarPorNome(String nome) {
        Iterable<Produto> produtos = produtoRepository.findAllByNome(nome);
        return produtos;
    }

    public Produto buscarPorId(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        throw new RuntimeException("O produto não foi encontrado");
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setId(id);
            Produto produtoObjeto = salvarProduto(produto);

            return produtoObjeto;
        }
        throw new RuntimeException("O produto não foi encontrado");
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else
            throw new RuntimeException("O produto não foi encontrado");
    }


}
