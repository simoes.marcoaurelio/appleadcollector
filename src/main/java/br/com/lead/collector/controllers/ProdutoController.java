package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.management.RuntimeMBeanException;

@RestController
@RequestMapping("/produtos")

public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping // usar sem parametros de entrada, ou com localhost:8080/produtos?nome=conteúdo
    public Iterable<Produto> exibirTodos(@RequestParam(name = "nome", required = false) String nome) {
        if (nome != null) {
            Iterable<Produto> produtos = produtoService.buscarPorNome(nome);
            return produtos;
        }
        Iterable<Produto> produtos = produtoService.buscarTodos();
        return produtos;
    }

    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") int id, @RequestBody Produto produto) {
        try {
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable(name = "id") int id) {
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }



}
